<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Validator.class.php');
$check = new Validator();

require_once('modele/users.php');

$check->isValidUsername($_POST['pseudo']);
if (!checkValidUsername($db, $_POST['pseudo']))
	$check->setError("This username already exists.");
$check->isValidEmail($_POST['email']);
if (!checkValidUsername($db, $_POST['email']))
	$check->setError("This email address already exists.");
$check->isValidPassword($_POST['password'], $_POST['confirm']);

if (!$check->hasError()) {
	$id = createUser($db, array (
		'pseudo'	=> $_POST['pseudo'],
		'email'		=> $_POST['email'],
		'password'	=> $_POST['password']
	));
	$account = getUserById($db, $id);
	$start = 'http://' . str_replace("_register.php", "", $_SERVER[HTTP_HOST] . $_SERVER['REQUEST_URI']);
	$header = 'From: "Camagru" <aboucher@student.42.fr>' . "\r\n";
	$header .= 'Reply-To: <aboucher@student.42.fr>' . "\r\n";
	$header .= 'MIME-Version: 1.0' . "\r\n";
	$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$content = file_get_contents($start . 'emailings/activate.php?pseudo=' . $_POST['pseudo'] . '&token=' . $account['token'] . "&url=" . $start);
	mail($_POST['email'], "Camagru - Activate your account", $content, $header);
}

if ($check->hasError())
	echo implode(';', $check->getErrors());

?>
