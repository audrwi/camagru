<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Validator.class.php');
$check = new Validator();

require_once('modele/users.php');

$user = getUserByEmail($db, $_POST['email']);
$check->isValidEmail($_POST['email']);
if (checkValidEmail($db, $_POST['email']))
	$check->setError("This email address doesn't exist.");
if (!$check->hasError() && $user['active'] == 0)
	$check->setError("This account is not activated.");
else if (!$check->hasError() && $user['active'] == 2)
	$check->setError("This account has been deleted.");

if (!$check->hasError()) {
	$token = changeUserToken($db, $user['id']);
	updateActive($db, array ('id' => $user['id'], 'active' => 3));
	$start = 'http://' . str_replace("_sendForgotEmail.php", "", $_SERVER[HTTP_HOST] . $_SERVER['REQUEST_URI']);
	$header = 'From: "Camagru" <aboucher@student.42.fr>' . "\r\n";
	$header .= 'Reply-To: <aboucher@student.42.fr>' . "\r\n";
	$header .= 'MIME-Version: 1.0' . "\r\n";
	$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$content = file_get_contents($start . 'emailings/forgot.php?pseudo=' . $user['pseudo'] . '&email=' . $_POST['email'] . '&token=' . $token . '&url=' . $start);
	mail($_POST['email'], "Camagru - Reset your password", $content, $header);
}

if ($check->hasError())
	echo implode(';', $check->getErrors());

?>
