navigator.getUserMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.mediaDevices.getUserMedia);

var url, posX, posY, inContainer, nb, imgWidth, imgRotate, webcamStream;

function noWebcam() {
	document.getElementById('container').style.display = "none";
	document.getElementById('moves').style.display = "none";
	document.getElementById('takePicture').style.display = "none";
	document.getElementById('clear').style.display = "none";
}

function startCam() {
	if (navigator.getUserMedia) {
		navigator.getUserMedia ({ video: true, audio: false }, function(localMediaStream) {
			var video	= document.getElementById('video'),
				button	= document.getElementById('takePicture');
			video.src = window.URL.createObjectURL(localMediaStream);
			video.style.webkitTransform = "scale(-1, 1)";
			video.style.MozTransform = "scale(-1, 1)";
			video.style.OTransform = "scale(-1, 1)";
			video.style.transform = "scale(-1, 1)";
			video.play();
			button.style.display = "block";
			webcamStream = localMediaStream;
		}, function(err) { noWebcam(); });
	} else { noWebcam(); }
}

startCam();

function takePicture() {
	var video	= document.getElementById('video'),
		canvas	= document.getElementById('canvas');
	canvas.width = video.getBoundingClientRect().width;
	canvas.height = video.getBoundingClientRect().height;
	canvas.getContext('2d').translate(canvas.width / 2, canvas.height / 2);
	canvas.getContext('2d').scale(-1, 1);
	canvas.getContext('2d').translate(-canvas.width / 2, -canvas.height / 2);
	canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
	var elements	= document.querySelectorAll('#container > img'),
		i			= 0;
	[].forEach.call(elements, function(elem) {
		rotateAndDrawImage(canvas.getContext('2d'), elem, getCurrentRotation(elem), i);
		i++;
	});
	if (i > 0) {
		var data = { image: canvas.toDataURL('image/png').split(',')[1] };
		ajax.post('_newImage.php', data, function(r) {
			document.getElementById('my-photos').innerHTML = r + document.getElementById('my-photos').innerHTML;
			document.getElementById('success').innerHTML = "<p>Image successfully uploaded.</p>";
			document.getElementById('errors').innerHTML = "";
		});
	} else {
		document.getElementById('errors').innerHTML = "<p>You have to use some filters. Or else, it's useless.</p>";
		document.getElementById('success').innerHTML = "";
	}
}

function rotateAndDrawImage(context, image, angle, i) {
	var angleInRad	= angle * (Math.PI / 180),
		x			= image.width / 2,
		y			= image.height / 2,
		canvas		= document.getElementById('canvas');
	if (!i) {
		context.translate(canvas.width / 2, canvas.height / 2);
		context.scale(-1, 1);
		context.translate(-canvas.width / 2, -canvas.height / 2);
	}
	context.translate(image.offsetLeft + x, image.offsetTop + y);
	context.rotate(angleInRad);
	context.drawImage(image, -x, -y, image.width, image.height);
	context.rotate(-angleInRad);
	context.translate(-image.offsetLeft - x, -image.offsetTop - y);
}

function getPosition(e) {
	posX = e.clientX - document.getElementById('container').getBoundingClientRect().left;
	posY = e.clientY - document.getElementById('container').getBoundingClientRect().top;
}

function countFilters() {
	var elements = document.querySelectorAll('#container > img');
	nb = 0;
	[].forEach.call(elements, function(elem) { nb++; });
	return nb;
}

function changePointerEvents(pointerEvent) {
	var elements = document.querySelectorAll('#container > img');
	[].forEach.call(elements, function(elem) {
		if (elem.className != "delete")
			elem.style.pointerEvents = pointerEvent;
	});
}

function drag(elem) {
	var container	= document.getElementById('container');
	url = elem.src;
	imgWidth = elem.width + 4;
	imgRotate = getCurrentRotation(elem);
	inContainer = (elem.parentNode == container);
	elem.className = (inContainer) ? "delete" : "";
	changePointerEvents("none");
	if (document.getElementById('moves'))
		document.getElementById('moves').style.display = "none";
	else
		document.getElementById('moves-2').style.display = "none";
}

function drop(e) {
	e.preventDefault();
	var container	= document.getElementById('container'),
		button		= document.getElementById('clear');
	if (inContainer) {
		var elements = document.querySelectorAll('#container > img');
		[].forEach.call(elements, function(elem) {
			if (elem.className == "delete")
				elem.remove();
		});
	}
	container.innerHTML = container.innerHTML + '<img src="' + url + '" style="top:' + (posY - imgWidth / 2) + 'px; left:' + (posX - imgWidth / 2) + 'px; width: ' + imgWidth + 'px; transform: rotate(' + imgRotate + 'deg); -webkit-transform: rotate(' + imgRotate + 'deg); -moz-transform: rotate(' + imgRotate + 'deg); -o-transform: rotate(' + imgRotate + 'deg);" onclick="selectFilter(this);" draggable="true" ondragstart="drag(this);">';
	// startCam();
	document.getElementById('video').play();
	button.style.display = (countFilters() > 0) ? "block" : "none";
	changePointerEvents("auto");
}

function clearFilters() {
	var elements	= document.querySelectorAll('#container > img'),
		button		= document.getElementById('clear');
	[].forEach.call(elements, function(elem) {
		elem.remove();
	});
	button.style.display = (countFilters() > 0) ? "block" : "none";
	document.getElementById('moves').style.display = "none";
}

function selectFilter(e) {
	var elements	= document.querySelectorAll('#container > img'),
		i			= 0;
	[].forEach.call(elements, function(elem) {
		if (elem != e)
			elem.className = "";
	});
	e.className = (e.className == "selected") ? "" : "selected";
	[].forEach.call(elements, function(elem) {
		if (elem.className == "selected")
			i++;
	});
	document.getElementById('moves').style.display = (i == 1) ? "block" : "none";
	if (i != 1) {
		[].forEach.call(elements, function(elem) {
			elem.className = "";
		});
	} else {
		document.getElementById('moves').style.left = (window.innerWidth > 970) ? document.getElementById('container').getBoundingClientRect().left - 90 + "px" : ((window.innerWidth > 640) ? 50 + "px" : 25 + "px");
	}
}

function changeSize(diff) {
	var image			= document.querySelectorAll('#container > img.selected')[0],
		originalWidth	= image.width,
		originalPosX	= image.offsetTop,
		originalPosY	= image.offsetLeft,
		scale			= document.getElementById('container').getBoundingClientRect().width / 25;
	image.style.width = (diff == 1) ? (originalWidth + scale) + "px" : ((originalWidth > scale + 1) ? originalWidth - scale + "px" : originalWidth + "px");
	image.style.top = (diff == 1) ? originalPosX - scale / 2 + "px" : ((originalWidth > scale + 1) ? (originalPosX + scale / 2 + "px") : originalPosX + "px");
	image.style.left = (diff == 1) ? originalPosY - scale / 2 + "px" : ((originalWidth > scale + 1) ? (originalPosY + scale / 2 + "px") : originalPosY + "px");
}

function doRotate(diff) {
	var image		= document.querySelectorAll('#container > img.selected')[0],
		angle		= getCurrentRotation(image),
		newAngle	= (diff == 1) ? angle - 20 : (angle + 20);
	image.style.webkitTransform = "rotate(" + newAngle + "deg)";
	image.style.MozTransform = "rotate(" + newAngle + "deg)";
	image.style.OTransform = "rotate(" + newAngle + "deg)";
	image.style.transform = "rotate(" + newAngle + "deg)";
}

function deleteElem() {
	var image	= document.querySelectorAll('#container > img.selected')[0],
		button	= document.getElementById('clear');
	image.remove();
	button.style.display = (countFilters() > 0) ? "block" : "none";
	document.getElementById('moves').style.display = "none";
}

function getCurrentRotation(el) {
	var st	= window.getComputedStyle(el, null),
		tr	= st.getPropertyValue("-webkit-transform") || st.getPropertyValue("-moz-transform") || st.getPropertyValue("-o-transform") || st.getPropertyValue("transform") || "none";
	if (tr != "none") {
		var values = tr.split('(')[1];
		values = values.split(')')[0];
		values = values.split(',');
		var a = values[0],
			b = values[1],
			radians = Math.atan2(b, a),
			angle = Math.round(radians * (180 / Math.PI));
	} else { var angle = 0; }
	return angle;
}
