function getPicture() {
	var img		= document.getElementById('image'),
		picture	= document.getElementById('picture');
	document.getElementById('success').innerHTML = "";
	document.getElementById('errors').innerHTML = "<p>Unable to load your image.</p>";
	if (img.files && img.files[0] && img.files[0].type.match('image.*')) {
		var reader	= new FileReader();
		reader.onload = function(e) {
			var image	= new Image();
			image.onload = function() {
				picture.src = e.target.result;
				webcamStream.getVideoTracks()[0].stop();
				document.getElementById('errors').innerHTML = "";
				document.getElementById('container-2').style.display = "block";
				document.getElementById('takePicture-2').style.display = "block";
				document.getElementById('takePicture').remove();
				document.getElementById('clear').remove();
				document.getElementById('container').remove();
				document.getElementById('moves').remove();
				setGoodSize();
			};
			image.src = e.target.result;
		};
		reader.readAsDataURL(img.files[0]);
	} else
		document.getElementById('errors').innerHTML = "";
	img.value = "";
	document.getElementById('text-content').innerText = "Choose an image...";
}

function takePicture2() {
	var img		= document.getElementById('picture'),
		canvas	= document.getElementById('canvas');
	canvas.width = img.width;
	canvas.height = img.height;
	canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);
	var elements	= document.querySelectorAll('#container-2 > img:not(#picture)'),
		i			= 0;
	[].forEach.call(elements, function(elem) {
		rotateAndDrawImage2(canvas.getContext('2d'), elem, getCurrentRotation(elem));
		i++;
	});
	if (i > 0) {
		var data = { image: canvas.toDataURL('image/png').split(',')[1] };
		ajax.post('_newImage.php', data, function(r) {
			document.getElementById('my-photos').innerHTML = r + document.getElementById('my-photos').innerHTML;
			document.getElementById('success').innerHTML = "<p>Image successfully uploaded.</p>";
			document.getElementById('errors').innerHTML = "";
		});
	} else {
		document.getElementById('errors').innerHTML = "<p>You have to use some filters. Or else, it's useless.</p>";
		document.getElementById('success').innerHTML = "";
	}
}

function rotateAndDrawImage2(context, image, angle) {
	var angleInRad	= angle * (Math.PI / 180),
		x			= image.width / 2,
		y			= image.height / 2,
		canvas		= document.getElementById('canvas');
	context.translate(image.offsetLeft + x, image.offsetTop + y);
	context.rotate(angleInRad);
	context.drawImage(image, -x, -y, image.width, image.height);
	context.rotate(-angleInRad);
	context.translate(-image.offsetLeft - x, -image.offsetTop - y);
}

function changePointerEvents2(pointerEvent) {
	var elements = document.querySelectorAll('#container-2 > img:not(#picture)');
	[].forEach.call(elements, function(elem) {
		if (elem.className != "delete")
			elem.style.pointerEvents = pointerEvent;
	});
}

function getPosition2(e) {
	posX = e.clientX - document.getElementById('container-2').getBoundingClientRect().left;
	posY = e.clientY - document.getElementById('container-2').getBoundingClientRect().top;
}

function countFilters2() {
	var elements = document.querySelectorAll('#container-2 > img:not(#picture)');
	nb = 0;
	[].forEach.call(elements, function(elem) { nb++; });
	return nb;
}

function drag2(elem) {
	var container	= document.getElementById('container-2');
	url = elem.src;
	imgWidth = elem.width + 4;
	imgRotate = getCurrentRotation(elem);
	inContainer = (elem.parentNode == container);
	elem.className = (inContainer) ? "delete" : "";
	changePointerEvents2("none");
	document.getElementById('moves-2').style.display = "none";
}

function drop2(e) {
	e.preventDefault();
	var container	= document.getElementById('container-2'),
		button		= document.getElementById('clear-2');
	if (inContainer) {
		var elements = document.querySelectorAll('#container-2 > img:not(#picture)');
		[].forEach.call(elements, function(elem) {
			if (elem.className == "delete")
				elem.remove();
		});
	}
	container.innerHTML = container.innerHTML + '<img src="' + url + '" style="top:' + (posY - imgWidth / 2) + 'px; left:' + (posX - imgWidth / 2) + 'px; width: ' + imgWidth + 'px; transform: rotate(' + imgRotate + 'deg); -webkit-transform: rotate(' + imgRotate + 'deg); -moz-transform: rotate(' + imgRotate + 'deg); -o-transform: rotate(' + imgRotate + 'deg);" onclick="selectFilter2(this);" draggable="true" ondragstart="drag2(this);">';
	button.style.display = (countFilters2() > 0) ? "block" : "none";
	changePointerEvents2("auto");
}

function selectFilter2(e) {
	var elements	= document.querySelectorAll('#container-2 > img:not(#picture)'),
		i			= 0;
	[].forEach.call(elements, function(elem) {
		if (elem != e)
			elem.className = "";
	});
	e.className = (e.className == "selected") ? "" : "selected";
	[].forEach.call(elements, function(elem) {
		if (elem.className == "selected")
			i++;
	});
	document.getElementById('moves-2').style.display = (i == 1) ? "block" : "none";
	if (i != 1) {
		[].forEach.call(elements, function(elem) {
			elem.className = "";
		});
	} else {
		document.getElementById('moves-2').style.left = (window.innerWidth > 970) ? document.getElementById('container-2').getBoundingClientRect().left - 90 + "px" : ((window.innerWidth > 640) ? 50 + "px" : 25 + "px");
	}
}

function changeSize2(diff) {
	var image			= document.querySelectorAll('#container-2 > img.selected')[0],
		originalWidth	= image.width,
		originalPosX	= image.offsetTop,
		originalPosY	= image.offsetLeft,
		scale			= document.getElementById('container-2').getBoundingClientRect().width / 25;
	image.style.width = (diff == 1) ? (originalWidth + scale) + "px" : ((originalWidth > scale + 1) ? originalWidth - scale + "px" : originalWidth + "px");
	image.style.top = (diff == 1) ? originalPosX - scale / 2 + "px" : ((originalWidth > scale + 1) ? (originalPosX + scale / 2 + "px") : originalPosX + "px");
	image.style.left = (diff == 1) ? originalPosY - scale / 2 + "px" : ((originalWidth > scale + 1) ? (originalPosY + scale / 2 + "px") : originalPosY + "px");
}

function doRotate2(diff) {
	var image		= document.querySelectorAll('#container-2 > img.selected')[0],
		angle		= getCurrentRotation(image),
		newAngle	= (diff == 1) ? angle - 20 : (angle + 20);
	image.style.webkitTransform = "rotate(" + newAngle + "deg)";
	image.style.MozTransform = "rotate(" + newAngle + "deg)";
	image.style.OTransform = "rotate(" + newAngle + "deg)";
	image.style.transform = "rotate(" + newAngle + "deg)";
}

function deleteElem2() {
	var image	= document.querySelectorAll('#container-2 > img.selected')[0],
		button	= document.getElementById('clear-2');
	image.remove();
	button.style.display = (countFilters2() > 0) ? "block" : "none";
	document.getElementById('moves-2').style.display = "none";
}

function clearFilters2() {
	var elements	= document.querySelectorAll('#container-2 > img:not(#picture)'),
		button		= document.getElementById('clear-2');
	[].forEach.call(elements, function(elem) {
		elem.remove();
	});
	button.style.display = (countFilters2() > 0) ? "block" : "none";
	document.getElementById('moves-2').style.display = "none";
}
