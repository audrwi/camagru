var ajax = {};

ajax.x = function () {
	if (typeof XMLHttpRequest !== 'undefined')
		return new XMLHttpRequest();
	var versions	= [
			"MSXML2.XmlHttp.6.0",
			"MSXML2.XmlHttp.5.0",
			"MSXML2.XmlHttp.4.0",
			"MSXML2.XmlHttp.3.0",
			"MSXML2.XmlHttp.2.0",
			"Microsoft.XmlHttp"
		],
		xhr;
	for (var i = 0; i < versions.length; i++) {
		try {
			xhr = new ActiveXObject(versions[i]);
			break;
		} catch (e) { }
	}
	return xhr;
};

ajax.send = function (url, callback, method, data, async) {
	if (async === undefined)
		async = true;
	var x	= ajax.x();
	x.open(method, url, async);
	x.onreadystatechange = function () {
		if (x.readyState == 4)
			callback(x.responseText);
	};
	if (method == 'POST')
		x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	x.send(data);
	return x.responseText;
};

ajax.get = function (url, data, callback, async) {
	var query	= [];
	for (var key in data)
		query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async);
};

ajax.post = function (url, data, callback, async) {
	var query	= [];
	for (var key in data)
		query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	ajax.send(url, callback, 'POST', query.join('&'), async);
};

function registerAction() {
	var infos	= {
			pseudo:		document.getElementById('pseudo').value,
			email:		document.getElementById('email').value,
			password:	document.getElementById('password').value,
			confirm:	document.getElementById('confirm').value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_register.php', infos, function(r) {
		if (r.length) {
			var res = r.split(';');
			errors.innerHTML = "";
			success.innerHTML = "";
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else {
			errors.innerHTML = "";
			success.innerHTML = "<p>You successfully created an account.</p><p>Check your emails to activate it.</p>";
		}
	});
}

function loginAction() {
	var infos	= {
			username:	document.getElementById('username').value,
			password:	document.getElementById('password').value
		},
		errors	= document.getElementById('errors');
	ajax.post('_login.php', infos, function(r) {
		if (r.length) {
			var res = r.split(';');
			errors.innerHTML = "";
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			window.location.replace('./');
	});
}

function toggle_visibility(id) {
	var e	= document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}

function toggle_modal(id) {
	var e			= document.getElementById(id),
		bg			= document.getElementById('bg_modal'),
		htmlElement	= document.getElementsByTagName("html")[0];
	if(e.style.display == "block") {
		e.style.display = "none";
		bg.style.display = "none";
		document.body.style.overflow = "auto";
		htmlElement.style.overflow = "auto";
	} else {
		e.style.display = "block";
		bg.style.display = "block";
		document.body.style.overflow = "hidden";
		htmlElement.style.overflow = "hidden";
	}
}

function deleteAccount() {
	ajax.post('_deleteAccount.php', {}, function() {
		window.location.replace('logout');
	});
}

function likeOrUnlikeImage(e, id) {
	var infos	= {
			image_id:	id
		};
	ajax.post('_likeOrUnlike.php', infos, function(r) {
		e.innerHTML = '<span class="nb-like">' + r + '</span> <span class="icon-heart"></span>';
		e.className = (e.className == 'like liked') ? 'like' : 'like liked';
	});
}

function deleteImage(e, id) {
	var infos	= {
			image_id:	id
		};
	ajax.post('_deleteImage.php', infos, function() {
		e.parentElement.parentElement.parentElement.remove();
	});
}

function addComment() {
	var infos	= {
			message:	document.getElementById('message').value,
			image:		location.pathname.split('/image-')[1]
		},
		coms	= document.getElementById('comments'),
		nb		= document.getElementById('nb-comments');
	ajax.post('_addComment.php', infos, function(r){
		coms.innerHTML += r;
		nb.innerText = parseInt(nb.innerText) + parseInt(1);
		document.getElementById('message').value = "";
		if (document.getElementById('no-comment'))
			document.getElementById('no-comment').remove();
	});
}

function customFileButton(id) {
	var input	= document.getElementById(id);
	input.addEventListener('change', function(e) {
		var name	= e.target.value.split('\\').pop()
		document.getElementById('text-content').innerText = (name) ? name : "Choose an image...";
		if (!name)
			input.value = "";
	});
}

function sendForgotEmail() {
	var infos	= {
			email:	document.getElementById("email").value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_sendForgotEmail.php', infos, function(r) {
		errors.innerHTML = "";
		success.innerHTML = "";
		if (r.length) {
			var res = r.split(';');
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			success.innerHTML = "<p>An confirmation email has been sent to you.</p><p>Check your emails to choose a new password.</p>";
	});
}

function changePassword() {
	var infos	= {
			email:	document.getElementById("email").value,
			token:	document.getElementById("token").value,
			pass:	document.getElementById("password").value,
			conf:	document.getElementById("confirm").value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_changePassword.php', infos, function(r) {
		errors.innerHTML = "";
		success.innerHTML = "";
		if (r.length) {
			var res = r.split(';');
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			success.innerHTML = "<p>Your password has been successfully changed.</p><p>You can login with the new one until now.</p>";
	});
}

function setGoodSize() {
	if (document.getElementById('container')) {
		document.getElementById('container').style.width = (window.innerWidth > 970) ? 640 + "px" : ((window.innerWidth > 640) ? window.innerWidth - 200 + "px" : window.innerWidth - 150 + "px");
		if (document.getElementById('container').getBoundingClientRect().width < 640)
			clearFilters();
	} else if (document.getElementById('container-2')) {
		document.getElementById('container-2').style.width = (window.innerWidth > 970) ? 640 + "px" : ((window.innerWidth > 640) ? window.innerWidth - 200 + "px" : window.innerWidth - 150 + "px");
		if (document.getElementById('container').getBoundingClientRect().width < 640)
			clearFilters2();
	}
}

window.onresize = function() {
	setGoodSize();
}

window.onload = function() {
	setGoodSize();
}
var ajax = {};

ajax.x = function () {
	if (typeof XMLHttpRequest !== 'undefined')
		return new XMLHttpRequest();
	var versions	= [
			"MSXML2.XmlHttp.6.0",
			"MSXML2.XmlHttp.5.0",
			"MSXML2.XmlHttp.4.0",
			"MSXML2.XmlHttp.3.0",
			"MSXML2.XmlHttp.2.0",
			"Microsoft.XmlHttp"
		],
		xhr;
	for (var i = 0; i < versions.length; i++) {
		try {
			xhr = new ActiveXObject(versions[i]);
			break;
		} catch (e) { }
	}
	return xhr;
};

ajax.send = function (url, callback, method, data, async) {
	if (async === undefined)
		async = true;
	var x	= ajax.x();
	x.open(method, url, async);
	x.onreadystatechange = function () {
		if (x.readyState == 4)
			callback(x.responseText);
	};
	if (method == 'POST')
		x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	x.send(data);
	return x.responseText;
};

ajax.get = function (url, data, callback, async) {
	var query	= [];
	for (var key in data)
		query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async);
};

ajax.post = function (url, data, callback, async) {
	var query	= [];
	for (var key in data)
		query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	ajax.send(url, callback, 'POST', query.join('&'), async);
};

function registerAction() {
	var infos	= {
			pseudo:		document.getElementById('pseudo').value,
			email:		document.getElementById('email').value,
			password:	document.getElementById('password').value,
			confirm:	document.getElementById('confirm').value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_register.php', infos, function(r) {
		if (r.length) {
			var res = r.split(';');
			errors.innerHTML = "";
			success.innerHTML = "";
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else {
			errors.innerHTML = "";
			success.innerHTML = "<p>You successfully created an account.</p><p>Check your emails to activate it.</p>";
		}
	});
}

function loginAction() {
	var infos	= {
			username:	document.getElementById('username').value,
			password:	document.getElementById('password').value
		},
		errors	= document.getElementById('errors');
	ajax.post('_login.php', infos, function(r) {
		if (r.length) {
			var res = r.split(';');
			errors.innerHTML = "";
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			window.location.replace('./');
	});
}

function toggle_visibility(id) {
	var e	= document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}

function toggle_modal(id) {
	var e			= document.getElementById(id),
		bg			= document.getElementById('bg_modal'),
		htmlElement	= document.getElementsByTagName("html")[0];
	if(e.style.display == "block") {
		e.style.display = "none";
		bg.style.display = "none";
		document.body.style.overflow = "auto";
		htmlElement.style.overflow = "auto";
	} else {
		e.style.display = "block";
		bg.style.display = "block";
		document.body.style.overflow = "hidden";
		htmlElement.style.overflow = "hidden";
	}
}

function deleteAccount() {
	ajax.post('_deleteAccount.php', {}, function() {
		window.location.replace('logout');
	});
}

function likeOrUnlikeImage(e, id) {
	var infos	= {
			image_id:	id
		};
	ajax.post('_likeOrUnlike.php', infos, function(r) {
		e.innerHTML = '<span class="nb-like">' + r + '</span> <span class="icon-heart"></span>';
		e.className = (e.className == 'like liked') ? 'like' : 'like liked';
	});
}

function deleteImage(e, id) {
	var infos	= {
			image_id:	id
		};
	ajax.post('_deleteImage.php', infos, function() {
		e.parentElement.parentElement.parentElement.remove();
	});
}

function addComment() {
	var infos	= {
			message:	document.getElementById('message').value,
			image:		location.pathname.split('/image-')[1]
		},
		coms	= document.getElementById('comments'),
		nb		= document.getElementById('nb-comments');
	ajax.post('_addComment.php', infos, function(r){
		coms.innerHTML += r;
		nb.innerText = parseInt(nb.innerText) + parseInt(1);
		document.getElementById('message').value = "";
		if (document.getElementById('no-comment'))
			document.getElementById('no-comment').remove();
	});
}

function customFileButton(id) {
	var input	= document.getElementById(id);
	input.addEventListener('change', function(e) {
		var name	= e.target.value.split('\\').pop()
		document.getElementById('text-content').innerText = (name) ? name : "Choose an image...";
		if (!name)
			input.value = "";
	});
}

function sendForgotEmail() {
	var infos	= {
			email:	document.getElementById("email").value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_sendForgotEmail.php', infos, function(r) {
		errors.innerHTML = "";
		success.innerHTML = "";
		if (r.length) {
			var res = r.split(';');
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			success.innerHTML = "<p>An confirmation email has been sent to you.</p><p>Check your emails to choose a new password.</p>";
	});
}

function changePassword() {
	var infos	= {
			email:	document.getElementById("email").value,
			token:	document.getElementById("token").value,
			pass:	document.getElementById("password").value,
			conf:	document.getElementById("confirm").value
		},
		errors	= document.getElementById('errors'),
		success	= document.getElementById('success');
	ajax.post('_changePassword.php', infos, function(r) {
		errors.innerHTML = "";
		success.innerHTML = "";
		if (r.length) {
			var res = r.split(';');
			res.forEach(function(value) {
				errors.innerHTML += "<p>" + value + "</p>";
			});
		} else
			success.innerHTML = "<p>Your password has been successfully changed.</p><p>You can login with the new one until now.</p>";
	});
}

function setGoodSize() {
	if (document.getElementById('container')) {
		document.getElementById('container').style.width = (window.innerWidth > 970) ? 640 + "px" : ((window.innerWidth > 640) ? window.innerWidth - 200 + "px" : window.innerWidth - 150 + "px");
		document.getElementById('container').style.height = document.getElementById('video').getBoundingClientRect().height + "px";
		if (document.getElementById('container').getBoundingClientRect().width < 640)
			clearFilters();
	} else if (document.getElementById('container-2')) {
		document.getElementById('container-2').style.width = (window.innerWidth > 970) ? 640 + "px" : ((window.innerWidth > 640) ? window.innerWidth - 200 + "px" : window.innerWidth - 150 + "px");
		document.getElementById('container-2').style.height = document.getElementById('picture').getBoundingClientRect().height + "px";
		if (document.getElementById('container').getBoundingClientRect().width < 640)
			clearFilters2();
	}
}

window.onresize = function() {
	setGoodSize();
}

window.onload = function() {
	setGoodSize();
}
