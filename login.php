<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Form.class.php');
$form = new Forms();

if ($_SESSION['logged'] == True) {
	header("Location: ./");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = 'Login';
		$description = 'Login to use the app.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'login';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h2>Log in</h2>
		<form method="POST" onsubmit="loginAction(); return false;" action="">
			<div id="errors"></div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "text", "name" => "username", "value" => isset($_POST['pseudo']) ? $_POST['pseudo'] : "", "placeholder" => "Your pseudo or email", "id" => "username")); ?>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "password", "name" => "password", "value" => "", "placeholder" => "Your password", "id" => "password")); ?>
			</div>
			<?php echo $form->createInput(array("type" => "submit", "value" => "Log in")); ?>
		</form>
		<a href="forgot">Forgot your password ?</a>
		<p>You don't have an account yet ? You have to <a href="register">register</a>.</p>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
