<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Validator.class.php');
$check = new Validator();

require_once('modele/users.php');

$user = checkUserExists($db, array ('login' => $_POST['username']));

if (!isset($_POST['username']) || $_POST['username'] == "")
	$check->setError("Username required.");
else if ($user == False)
	$check->setError("This username doesn't exists.");
else if (!isRightPassword($user['password'], $_POST['password']) && $_POST['password'] != "")
	$check->setError("Wrong password.");
if (!isset($_POST['password']) || $_POST['password'] == "")
	$check->setError("Password required.");

if (!$check->hasError() && $user['active'] == 0)
	$check->setError("This account is not activated.");
else if (!$check->hasError() && $user['active'] == 2)
	$check->setError("This account has been deleted.");

if ($check->hasError())
	echo implode(';', $check->getErrors());
else {
	session_start();
	$_SESSION['logged'] = True;
	$_SESSION['logged_user_id'] = $user['id'];
}

?>
