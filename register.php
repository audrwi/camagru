<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Form.class.php');
$form = new Forms();

if ($_SESSION['logged'] == True) {
	header("Location: ./");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = 'Register';
		$description = 'Create an account to use the app.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'register';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h2>Create an account</h2>
		<form method="POST" onsubmit="registerAction(); return false;" action="">
			<div id="errors"></div>
			<div id="success"></div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "text", "name" => "pseudo", "value" => isset($_POST['pseudo']) ? $_POST['pseudo'] : "", "placeholder" => "Your pseudo", "id" => "pseudo")); ?><!--
				 --><p>*<span class="hide">20 characters maximum.<br>Only letters and numbers.</span></p>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "text", "name" => "email", "value" => isset($_POST['email']) ? $_POST['email'] : "", "placeholder" => "Your email address", "id" => "email")); ?><!--
				 --><p>*<span class="hide">A valid email address.</span></p>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "password", "name" => "password", "value" => "", "placeholder" => "Your password", "id" => "password")); ?><!--
				 --><p>*<span class="hide">Between 8 and 20 characters.</span></p>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "password", "name" => "password_confirm", "value" => "", "placeholder" => "Confirm your password", "id" => "confirm")); ?><!--
				 --><p>*</p>
			</div>
			<?php echo $form->createInput(array("type" => "submit", "value" => "Register")); ?>
		</form>
		<p>You already have an account ? Just <a href="login">login</a>.</p>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
