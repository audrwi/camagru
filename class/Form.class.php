<?php

require_once('class/traits/Doc.trait.php');

Class Forms {

	use Doc;

	private $_data;

	public function __construct(array $data = NULL) {
		$this->_data = $data;
	}

	public function createInput(array $attributes) {
		$input = "<input ";
		foreach ($attributes as $key => $value) {
			$input .= $key . '="' . $value . '" ';
		}
		$input .= ">";
		return $input;
	}

	public function __destruct() {
		$this->_data = NULL;
	}

}

?>
