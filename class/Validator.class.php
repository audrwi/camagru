<?php

require_once('class/traits/Doc.trait.php');

Class Validator {

	use Doc;

	private $_errors;

	public function __construct(array $errors = NULL) {
		$this->_errors = $errors;
	}

	public function isValidEmail($email) {
		$regex = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
		if ($email == "")
			$this->_errors[] = "Email address required.";
		else if (!preg_match($regex, $email))
			$this->_errors[] = "Invalid email address.";
	}

	public function isValidUsername($username) {
		$regex = '/^[a-zA-Z0-9]{1,20}$/';
		if ($username == "")
			$this->_errors[] = "Username required.";
		else if (!preg_match($regex, $username))
			$this->_errors[] = "Invalid username.";
	}

	public function isValidPassword($password, $confirm) {
		$regex = '/^.{8,20}$/';
		if ($password == "" || $confirm == "")
			$this->_errors[] = "Password and confirmation required.";
		else if (!preg_match($regex, $password))
			$this->_errors[] = "Invalid password.";
		else if ($password != $confirm)
			$this->_errors[] = "Wrong password confirmation.";
	}

	public function setError($error) {
		$this->_errors[] = $error;
	}

	public function hasError() {
		return (count($this->_errors) > 0);
	}

	public function getErrors() {
		return self::hasError() ? $this->_errors : NULL;
	}

	public function __destruct() {
		$this->_errors = NULL;
	}

}

?>
