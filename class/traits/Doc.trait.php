<?php

Trait Doc {

	public static function doc() {
		$filename = 'class/doc/' . __CLASS__ . '.doc.txt';
		$doc = '<- ' . __CLASS__ . ' ----------------------------------------------------------------------<br />';
		if (file_exists($filename))
			$doc .= file_get_contents($filename);
		$doc .= '---------------------------------------------------------------------- ' . __CLASS__ . ' -><br />';
		return $doc;
	}

}

?>
