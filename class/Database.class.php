<?php

require_once('class/traits/Doc.trait.php');
require_once('config/database.php');
require_once('config/setup.php');

Class Database {

	use Doc;

	private $_db;

	public function __construct($database) {
		session_start();
		try {
			$this->_db = new PDO('mysql:host=' . $DB_DSN . ';charset:UTF8', $DB_USER, $DB_PASSWORD);
			$this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if ($this->exist($database) === False)
				$this->_db->exec(create_db($database));
			else
				$this->_db->query("USE $database");
		} catch (PDOException $e) {
			echo 'File : ' . $e->getFile() . ' Line : ' . $e->getLine() . ' ' . $e->getMessage();
		}
	}

	public function __destruct() {
		$this->_db = NULL;
	}

	private function exist($database) {
		return ($this->_db->query("SHOW DATABASES LIKE '$database'")->fetch() == False) ? False : True;
	}

	public function getDB() {
		return $this->_db;
	}

}

?>
