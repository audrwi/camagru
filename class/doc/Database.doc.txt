
The Database class generates a connection to the chosen database with PDO and MySQL and it starts a session.

new Database();

It requires a PHP file /config/database.php in which you fill your database variables. For example :
$DB_DSN = 'localhost';
$DB_USER = 'root';
$DB_PASSWORD = 'root';
$DB_NAME = 'camagru';

The Database class checks if the chosen database exist and, if it doesn't, it creates the database before connecting to it.
For this functionality, it requires a PHP file (/config/install.php) which contains a 'create_db' function.
This function return the MySQL requests to create the database and its tables (it can also insert data into those tables).
