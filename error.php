<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');

if ($_SESSION['logged'] == True) {
	$me = getUserById($db, $_SESSION['logged_user_id']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = 'Error 404';
		$description = 'The page you\'re looking for doesn\'t exist.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'error';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h2><?php echo $title; ?></h2>
		<h3><?php echo $description; ?></h3>
		<img src="imgs/run.gif" alt="<?php echo $title; ?>">
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
