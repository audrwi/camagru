<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');

if ($_SESSION['logged'] == True) {
	$me = getUserById($db, $_SESSION['logged_user_id']);
} else {
	header("Location: ./");
	exit;
}

if (!isset($_GET['id']) || $_GET['id'] == "") {
	header("Location: ./");
	exit;
}

$user = checkUserExistsById($db, array ('id' => $_GET['id']));

if (isset($_POST)) {
	$error = "";
	if($_FILES['cover']['size'] != 0) {
		if($_FILES['cover']['error'] != 0)
			$error .= '<p>There was an error while downloading image.<br>Please, try again.</p>';
		$ext = array('jpg', 'jpeg', 'gif', 'png');
		$ext_sent = strrchr($_FILES['cover']['name'], '.');
		$ext_sent = substr($ext_sent, 1);
		$ext_sent = strtolower($ext_sent);
		$info = getimagesize($_FILES['cover']['tmp_name']);
		if(!in_array($ext_sent, $ext))
			$error .= '<p>Your image has not a good extension.<br>Please use one of those : .jpg, .jpeg , .gif, .png</p>';
		else if ($info == False)
			$error .= '<p>Unable to determine image type of uploaded file.</p>';
		if ($error == "") {
			if ($user['cover'] != NULL)
				unlink('imgs/users/' . $user['cover']);
			$url = time() . '.' . $ext_sent;
			$long_url = 'imgs/users/' . $url;
			move_uploaded_file($_FILES['cover']['tmp_name'], $long_url);
			updateCover($db, array ('id' => $_GET['id'], 'cover' => $url));
			$user = getUserById($db, $_GET['id']);
			$me = getUserById($db, $_SESSION['logged_user_id']);
		}
	}
}

if ($user == False || $user['active'] == 0 || $user['active'] == 2) {
	header("Location: ./");
	exit;
} else {
	$mine = ($_GET['id'] == $_SESSION['logged_user_id']);
	require_once('class/Form.class.php');
	$form = new Forms();
}

$images = getGeneratedImagesRevOrder($db, $_GET['id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = $mine ? 'My profile' : ucfirst($user['pseudo']) . "'s profile";
		$description = $mine ? 'Change your informations.' : 'View the ' . ucfirst($user['pseudo']) . "'s profile";
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'profile';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<?php if ($mine) { ?>
			<h2>My profile : <span><?php echo $user['pseudo']; ?></span></h2>
			<form method="POST" enctype="multipart/form-data" action="profile-<?php echo $_GET['id']; ?>">
				<?php if (isset($error) && $error != "") { ?>
					<div id="errors"><?php echo $error; ?></div>
				<?php } else if (isset($success)) { ?>
					<div id="success"><?php echo $success; ?></div>
				<?php } ?>
				<label for="cover">Cover</label>
				<img src="imgs/users/<?php if ($user['cover'] != '') { echo $user['cover']; } else { echo 'placeholder.jpg'; } ?>" alt="<?php echo $user['pseudo']; ?>">
				<?php echo $form->createInput(array("type" => "file", "name" => "cover", "id" => "cover", "size" => "200", "accept" => "image/*")); ?>
				<label for="cover" onclick="customFileButton('cover');">
					<span class="icon-upload2"></span>
					<span id="text-content">Choose an image...</span>
				</label>
				<?php echo $form->createInput(array("type" => "submit", "value" => "Change my image")); ?>
			</form>
			<button onclick="toggle_modal('modal_delete');" class="delete">Delete my account</button>
			<div class="modal" id="modal_delete">
				<span class="icon-cross" onclick="toggle_modal('modal_delete');"></span>
				<h2>Delete my account</h2>
				<p>Are you sure ?</p>
				<div>
					<button onclick="deleteAccount();">Yes</button><!--
					 --><button onclick="toggle_modal('modal_delete');">No</button>
				</div>
			</div>
			<div id="bg_modal" onclick="toggle_modal('modal_delete');"></div>
		<?php } else { ?>
			<h2><?php echo ucfirst($user['pseudo']); ?></h2>
			<img src="imgs/users/<?php if ($user['cover'] != '') { echo $user['cover']; } else { echo 'placeholder.jpg'; } ?>" alt="<?php echo $user['pseudo']; ?>">
		<?php } if (count($images) != 0) { ?>
		<div class="my-photos">
			<h3><?php if ($mine) { echo 'My photos'; } else { 'His/her photos'; } ?></h3>
			<article>
				<div class="scrollbar">
					<?php
						foreach($images as $key => $image) {
							$username = getUserById($db, $image['user_id'])['pseudo'];
					?>
						<div>
							<a href="image-<?php echo $image['id']; ?>">
								<figure>
									<img src="imgs/generated/<?php echo $image['url']; ?>">
								</figure>
							</a><!--
							--><figcaption><p><?php if ($mine) { ?><span class="icon-cross" onclick="deleteImage(this, <?php echo $image["id"]; ?>);"></span><?php } ?></p><p><span class="like<?php if (isLikedByMe($db, array("user_id" => $_SESSION['logged_user_id'], "image_id" => $image['id']))) { echo ' liked'; } ?>" onclick="likeOrUnlikeImage(this, <?php echo $image["id"]; ?>);"><span class="nb-like"><?php echo getLikesById($db, $image['id']); ?></span> <span class="icon-heart"></span></span><span class="comment"><?php echo getNumberCommentsById($db, $image['id']); ?> <span class="icon-bubble"></span></span></p></figcaption>
						</div>
					<?php } ?>
				</div>
			</article>
		</div>
		<?php } ?>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
