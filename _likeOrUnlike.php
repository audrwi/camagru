<?php

session_start();

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/images.php');

$tab = array (
	"user_id"	=> $_SESSION['logged_user_id'],
	"image_id"	=> $_POST['image_id']
);

if (isLikedByMe($db, $tab))
	unlike($db, $tab);
else
	like($db, $tab);

echo getLikesById($db, $_POST['image_id']);

?>
