<?php

function relativeTime($time) {
	$d[0] = array(60,"minute");
	$d[1] = array(3600,"hour");
	$d[2] = array(86400,"day");
	$d[3] = array(604800,"week");
	$d[4] = array(2592000,"month");
	$d[5] = array(31104000,"year");
	$w = array();
	$return = "";
	$now = time();
	$diff = ($now - $time);
	$secondsLeft = $diff;
	for ($i = 5; $i > -1; $i--) {
		$w[$i] = intval($secondsLeft / $d[$i][0]);
		$secondsLeft -= ($w[$i] * $d[$i][0]);
		if ($w[$i] != 0)
			return abs($w[$i]) . " " . $d[$i][1] . (($w[$i] > 1) ? 's' : '') . " " . (($diff > 0) ? "ago" : "left");
	}
	return "a few seconds ago";
}

?>
