<?php

function create_db($dbname) {
	$query = "CREATE DATABASE IF NOT EXISTS $dbname;";
	$query .= "USE $dbname;";

	$query .= "CREATE TABLE IF NOT EXISTS users (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		pseudo VARCHAR(50) NOT NULL,
		email VARCHAR(200) NOT NULL,
		password VARCHAR(250) NOT NULL,
		cover VARCHAR(250),
		active BOOLEAN NOT NULL DEFAULT FALSE,
		token VARCHAR(100) NOT NULL,
		created_at DATETIME NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "CREATE TABLE IF NOT EXISTS categories (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		name VARCHAR(50) NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "CREATE TABLE IF NOT EXISTS selectable_images (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		category_id INT UNSIGNED NOT NULL,
		name VARCHAR(50) NOT NULL,
		url VARCHAR(250) NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "CREATE TABLE IF NOT EXISTS generated_images (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		user_id INT UNSIGNED NOT NULL,
		url VARCHAR(250) NOT NULL,
		created_at DATETIME NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "CREATE TABLE IF NOT EXISTS likes (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		user_id INT UNSIGNED NOT NULL,
		image_id INT UNSIGNED NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "CREATE TABLE IF NOT EXISTS comments (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		user_id INT UNSIGNED NOT NULL,
		image_id INT UNSIGNED NOT NULL,
		message TEXT NOT NULL,
		created_at DATETIME NOT NULL,
		PRIMARY KEY (id)
	);";

	$query .= "INSERT INTO users (pseudo, email, password, active, token, created_at) VALUES ('audrwi', 'aboucher@student.42.fr', 'b72b9b29464ef426157478613957b8732512258b15af3e54e5a861ce72eb6ea74cf7a6b0a49390f21256f6b52c9eee16816f0481e5cd83378e5f0177e78bee56', 1, 'c6686c969d003eec60c64d02b0f8b815', NOW());";

	$query .= "INSERT INTO categories (name) VALUES ('Birds'), ('Desert'), ('Farm'), ('Forest'), ('Home'), ('Jungle'), ('Sea');";

	$query .= "INSERT INTO selectable_images (category_id, name, url) VALUES
	(1, 'Budgerigar', 'birds/budgerigar.png'), (1, 'Canary', 'birds/canary.png'), (1, 'Eagle', 'birds/eagle.png'),
	(1, 'Hawk', 'birds/hawk.png'), (1, 'Owl', 'birds/owl.png'), (1, 'Parrot', 'birds/parrot.png'),
	(1, 'Robin', 'birds/robin.png'), (1, 'Toucan', 'birds/toucan.png'), (2, 'Antelope', 'desert/antelope.png'),
	(2, 'Camel', 'desert/camel.png'), (2, 'Fennec', 'desert/fennec.png'), (2, 'Fox', 'desert/fox.png'),
	(2, 'Hyena', 'desert/hyena.png'), (2, 'Scorpion', 'desert/scorpion.png'), (2, 'Snake', 'desert/snake.png'),
	(2, 'Suricate', 'desert/suricate.png'), (3, 'Chicken', 'farm/chicken.png'), (3, 'Cow', 'farm/cow.png'),
	(3, 'Horse', 'farm/horse.png'), (3, 'Pig', 'farm/pig.png'), (3, 'Rooster', 'farm/rooster.png'),
	(3, 'Sheep', 'farm/sheep.png'), (4, 'Bear', 'forest/bear.png'), (4, 'Deer', 'forest/deer.png'),
	(4, 'Fox', 'forest/fox.png'), (4, 'Hedgehog', 'forest/hedgehog.png'), (4, 'Owl', 'forest/owl.png'),
	(4, 'Squirrel', 'forest/squirrel.png'), (4, 'Wild Boar', 'forest/wild_boar.png'), (4, 'Wolf', 'forest/wolf.png'),
	(5, 'Cat', 'home/cat.png'), (5, 'Dog', 'home/dog.png'), (5, 'Ferret', 'home/ferret.png'),
	(5, 'Hamster', 'home/hamster.png'), (5, 'Rabbit', 'home/rabbit.png'), (6, 'Elephant', 'jungle/elephant.png'),
	(6, 'Gorilla', 'jungle/gorilla.png'), (6, 'Leopard', 'jungle/leopard.png'), (6, 'Monkey', 'jungle/monkey.png'),
	(6, 'Orang Utan', 'jungle/orang_utan.png'), (6, 'Panda', 'jungle/panda.png'), (6, 'Tiger', 'jungle/tiger.png'),
	(7, 'Hermit Crab', 'sea/hermit_crab.png'), (7, 'Jellyfish', 'sea/jellyfish.png'), (7, 'Octopus', 'sea/octopus.png'),
	(7, 'Penguin', 'sea/penguin.png'), (7, 'Sea Lion', 'sea/sea_lion.png'), (7, 'Starfish', 'sea/starfish.png'),
	(7, 'Turtle', 'sea/turtle.png'), (7, 'Walrus', 'sea/walrus.png'), (7, 'White Sea Lion', 'sea/white_sea_lion.png');";

	$query .= "INSERT INTO generated_images (user_id, url, created_at) VALUES
	(1, '1464367122-1.png', NOW()),
	(1, '1464632285-1.png', NOW()),
	(1, '1464633103-1.png', NOW()),
	(1, '1464885452-1.png', NOW()),
	(1, '1465217365-1.png', NOW()),
	(1, '1465846610-1.png', NOW()),
	(1, '1465996204-1.png', NOW()),
	(1, '1466103328-1.png', NOW()),
	(1, '1466176934-1.png', NOW()),
	(1, '1466263316-1.png', NOW()),
	(1, '1466347056-1.png', NOW());";

	$query .= "INSERT INTO likes (user_id, image_id) VALUES (1, 6), (1, 8), (1, 9), (1, 2);";

	$query .= "INSERT INTO comments (user_id, image_id, message, created_at) VALUES
	(1, 8, 'Comment elle est trop bonne !', NOW()),
	(1, 8, 'Kawaiiiiiiiiiiiii &lt;3', NOW()),
	(1, 6, 'Du pouleeeeeeeeetttttttt', NOW()),
	(1, 11, 'INVASION DE PINGOUINS Oo', NOW());";

	return $query;
}

?>
