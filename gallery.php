<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');

if ($_SESSION['logged'] == True) {
	$me = getUserById($db, $_SESSION['logged_user_id']);
} else {
	header("Location: ./");
	exit;
}

$images = getGeneratedImages($db);
$pages = 0;
$images_per_page = 12;
foreach ($images as $key => $image) {
	if ($key % $images_per_page == 0)
		$pages++;
}
if (isset($_GET['p'])) {
	if (is_int($_GET['p']) || $_GET['p'] < 2 || $_GET['p'] > $pages) {
		header("Location: ./");
		exit;
	}
}
$num = isset($_GET['p']) ? $_GET['p'] : 1;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = "Gallery";
		$description = "See all users' photos.";
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'gallery';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h1>All photos</h1>
		<article>
			<?php
				if (count($images) == 0)
					echo '<p class="no-response">No image found.</p>';
				else {
					foreach($images as $key => $image) {
						if ($key + 1 > ($num - 1) * $images_per_page && $key + 1 < $num * $images_per_page + 1) {
							$username = getUserById($db, $image['user_id'])['pseudo'];
			?>
				<div>
					<a href="image-<?php echo $image['id']; ?>">
						<figure>
							<img src="imgs/generated/<?php echo $image['url']; ?>">
						</figure>
					</a><!--
					--><figcaption><p>Created by <span class="username"><a href="profile-<?php echo $image['user_id']; ?>" alt="<?php echo $username; ?>"><?php echo $username; ?></a></span></p><p><span class="like<?php if (isLikedByMe($db, array("user_id" => $_SESSION['logged_user_id'], "image_id" => $image['id']))) { echo ' liked'; } ?>" onclick="likeOrUnlikeImage(this, <?php echo $image["id"]; ?>);"><span class="nb-like"><?php echo getLikesById($db, $image['id']); ?></span> <span class="icon-heart"></span></span><span class="comment"><?php echo getNumberCommentsById($db, $image['id']); ?> <span class="icon-bubble"></span></span></p></figcaption>
				</div>
			<?php } } } ?>
		</article>
		<?php if ($pages > 1) { ?>
			<article class="pagination">
				<?php if ($num > 3) { ?>
					<a href="gallery"><p>1</p></a>
					<p>...</p>
				<?php } else { ?>
					<a href="gallery" <?php if ($num == 1) { ?>class="current"<?php } ?>><p>1</p></a>
				<?php
					}
					$start = ($num > 3) ? $num - 1 : 2;
					$end = ($num < $pages - 2) ? $num + 1 : $pages - 1;
					for ($i = $start; $i <= $end; $i++) {
				?>
					<a href="gallery-p-<?php echo $i; ?>" <?php if ($num == $i) { ?>class="current"<?php } ?>"><p><?php echo $i; ?></p></a>
				<?php } if ($num < $pages - 2) { ?>
					<p>...</p>
					<a href="gallery-p-<?php echo $pages; ?>"><p><?php echo $pages; ?></p></a>
				<?php } else { ?>
					<a href="gallery-p-<?php echo $pages; ?>" <?php if ($num == $pages) { ?>class="current"<?php } ?>><p><?php echo $pages; ?></p></a>
				<?php } ?>
			</article>
		<?php } ?>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
