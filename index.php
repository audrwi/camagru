<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');

if ($_SESSION['logged'] == True) {
	$me = getUserById($db, $_SESSION['logged_user_id']);
	$images = getImagesByCategory($db);
	$images2 = getLatestGeneratedImages($db, $_SESSION['logged_user_id']);
} else {
	$images = getGeneratedImages($db);
	$pages = 0;
	$images_per_page = 6;
	foreach ($images as $key => $image) {
		if ($key % $images_per_page == 0)
			$pages++;
	}
	if (isset($_GET['p'])) {
		if (is_int($_GET['p']) || $_GET['p'] < 2 || $_GET['p'] > $pages) {
			header("Location: ./");
			exit;
		}
	}
	$num = isset($_GET['p']) ? $_GET['p'] : 1;
}

require_once('class/Form.class.php');
$form = new Forms();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = '';
		$description = 'The app to take pictures with the cutest animals and to share it.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'index';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?> <?php if ($_SESSION['logged'] == True) { echo "logged"; } ?>">
		<?php if ($_SESSION['logged'] == True) { ?>
		<article id="webcam">
			<div id="moves">
				<span class="icon-plus" onclick="changeSize(1);"></span>
				<span class="icon-minus" onclick="changeSize(0);"></span>
				<span class="icon-undo" onclick="doRotate(1);"></span>
				<span class="icon-redo" onclick="doRotate(0);"></span>
				<span class="icon-cross" onclick="deleteElem();"></span>
			</div>
			<div id="moves-2">
				<span class="icon-plus" onclick="changeSize2(1);"></span>
				<span class="icon-minus" onclick="changeSize2(0);"></span>
				<span class="icon-undo" onclick="doRotate2(1);"></span>
				<span class="icon-redo" onclick="doRotate2(0);"></span>
				<span class="icon-cross" onclick="deleteElem2();"></span>
			</div>
			<div id="errors"></div>
			<div id="success"></div>
			<div id="container">
				<video src="" id="video" ondrop="drop(event);" ondragover="event.preventDefault(); getPosition(event);"></video>
			</div>
			<button id="takePicture" onclick="takePicture();">Take picture</button>
			<button id="clear" onclick="clearFilters();">Clear</button>
			<canvas id="canvas"></canvas>
			<h2>With your picture</h2>
			<div class="no-webcam">
				<form method="POST" enctype="multipart/form-data" onsubmit="getPicture(); return false;" action="">
					<?php echo $form->createInput(array("type" => "file", "name" => "image", "id" => "image", "accept" => "image/*")); ?>
					<label for="image" onclick="customFileButton('image');">
						<span class="icon-upload2"></span>
						<span id="text-content">Choose an image...</span>
					</label>
					<?php echo $form->createInput(array("type" => "submit", "value" => "Upload a picture")); ?>
				</form>
				<div id="container-2">
					<img src="" id="picture" ondrop="drop2(event);" ondragover="event.preventDefault(); getPosition2(event);">
				</div>
				<button id="takePicture-2" onclick="takePicture2();">Take picture</button>
				<button id="clear-2" onclick="clearFilters2();">Clear</button>
			</div>
			<div class="my-photos">
				<h3>My latest photos</h3>
				<article>
					<div class="scrollbar" id="my-photos">
						<?php
							if (count($images2) == 0) {
								echo '<p class="no-response">No image found.</p>';
							} else {
								foreach($images2 as $key => $image) {
									$username = getUserById($db, $image['user_id'])['pseudo'];
						?>
							<div>
								<a href="image-<?php echo $image['id']; ?>">
									<figure>
										<img src="imgs/generated/<?php echo $image['url']; ?>">
									</figure>
								</a><!--
								--><figcaption><p><span class="icon-cross" onclick="deleteImage(this, <?php echo $image["id"]; ?>);"></span></p><p><span class="like<?php if (isLikedByMe($db, array("user_id" => $_SESSION['logged_user_id'], "image_id" => $image['id']))) { echo ' liked'; } ?>" onclick="likeOrUnlikeImage(this, <?php echo $image["id"]; ?>);"><span class="nb-like"><?php echo getLikesById($db, $image['id']); ?></span> <span class="icon-heart"></span></span><span class="comment"><?php echo getNumberCommentsById($db, $image['id']); ?> <span class="icon-bubble"></span></span></p></figcaption>
							</div>
						<?php } } ?>
					</div>
				</article>
			</div>
		</article><!--
		--><article class="sidebar">
		 	<div class="scrollbar">
				<?php
					foreach($images as $key => $image) {
						if (!isset($images[$key - 1]) || $image['category_id'] != $images[$key - 1]['category_id'])
							echo '<p class="category">' . $image['category_name'] . '</p>';
						echo '<div><figure><img src="imgs/selectable/' . $image['url'] . '" alt="' . $image['name'] . '" draggable="true" ondragstart="drag(this);"></figure>';
						echo '<p><span>' . $image['name'] . '</span></p></div>';
					}
				?>
			</div>
		</article>
		<?php } else { ?>
		<h1>All photos</h1>
		<h2>You have to login to like or comment a photo.</h2>
		<article>
			<?php
				if (count($images) == 0)
					echo '<p>No image found.</p>';
				else {
					foreach($images as $key => $image) {
						if ($key + 1 > ($num - 1) * $images_per_page && $key + 1 < $num * $images_per_page + 1) {
			?>
				<div>
					<figure>
						<img src="imgs/generated/<?php echo $image['url']; ?>">
					</figure><!--
					--><figcaption><p>Created by <span class="username"><?php echo getUserById($db, $image['user_id'])['pseudo']; ?></span></p><p><span class="like"><?php echo getLikesById($db, $image['id']); ?> <span class="icon-heart"></span></span><span class="comment"><?php echo getNumberCommentsById($db, $image['id']); ?> <span class="icon-bubble"></span></span></p></figcaption>
				</div>
			<?php } } } ?>
		</article>
		<?php if ($pages > 1) { ?>
			<article class="pagination">
				<?php if ($num > 3) { ?>
					<a href="./"><p>1</p></a>
					<p>...</p>
				<?php } else { ?>
					<a href="./" <?php if ($num == 1) { ?>class="current"<?php } ?>><p>1</p></a>
				<?php
					}
					$start = ($num > 3) ? $num - 1 : 2;
					$end = ($num < $pages - 2) ? $num + 1 : $pages - 1;
					for ($i = $start; $i <= $end; $i++) {
				?>
					<a href="p-<?php echo $i; ?>" <?php if ($num == $i) { ?>class="current"<?php } ?>"><p><?php echo $i; ?></p></a>
				<?php } if ($num < $pages - 2) { ?>
					<p>...</p>
					<a href="p-<?php echo $pages; ?>"><p><?php echo $pages; ?></p></a>
				<?php } else { ?>
					<a href="p-<?php echo $pages; ?>" <?php if ($num == $pages) { ?>class="current"<?php } ?>><p><?php echo $pages; ?></p></a>
				<?php } ?>
			</article>
		<?php } } ?>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
