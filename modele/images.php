<?php

function getImagesByCategory($db) {
	$request = $db->getDB()->prepare("SELECT selectable_images.category_id AS category_id, selectable_images.name AS name, selectable_images.url AS url, categories.name AS category_name FROM selectable_images, categories WHERE selectable_images.category_id = categories.id ORDER BY categories.id ASC, selectable_images.name ASC");
	$request->execute();
	return $request->fetchAll(PDO::FETCH_ASSOC);
}

function getGeneratedImages($db) {
	$request = $db->getDB()->prepare("SELECT * FROM generated_images ORDER BY created_at, id");
	$request->execute();
	return $request->fetchAll(PDO::FETCH_ASSOC);
}

function getGeneratedImagesRevOrder($db, $user_id) {
	$tab = array (
		'user'	=> $user_id
	);
	$request = $db->getDB()->prepare("SELECT * FROM generated_images WHERE user_id = :user ORDER BY created_at DESC, id DESC");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC);
}

function getLatestGeneratedImages($db, $user_id) {
	$tab = array (
		'user'	=> $user_id
	);
	$request = $db->getDB()->prepare("SELECT * FROM generated_images WHERE user_id = :user ORDER BY created_at DESC, id DESC LIMIT 4");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC);
}

function getLikesById($db, $id_image) {
	$tab = array (
		'id'	=> $id_image
	);
	$request = $db->getDB()->prepare("SELECT * FROM likes WHERE image_id = :id");
	$request->execute($tab);
	return $request->rowCount();
}

function isLikedByMe($db, $infos) {
	$tab = array (
		'user'	=> $infos['user_id'],
		'image'	=> $infos['image_id']
	);
	$request = $db->getDB()->prepare("SELECT * FROM likes WHERE user_id = :user AND image_id = :image");
	$request->execute($tab);
	return ($request->rowCount() > 0);
}

function like($db, $infos) {
	$tab = array (
		'user'	=> $infos['user_id'],
		'image'	=> $infos['image_id']
	);
	$request = $db->getDB()->prepare("INSERT INTO likes (user_id, image_id) VALUES (:user, :image)");
	$request->execute($tab);
}

function unlike($db, $infos) {
	$tab = array (
		'user'	=> $infos['user_id'],
		'image'	=> $infos['image_id']
	);
	$request = $db->getDB()->prepare("DELETE FROM likes WHERE user_id = :user AND image_id = :image");
	$request->execute($tab);
}

function getNumberCommentsById($db, $id_image) {
	$tab = array (
		'id'	=> $id_image
	);
	$request = $db->getDB()->prepare("SELECT * FROM comments WHERE image_id = :id");
	$request->execute($tab);
	return $request->rowCount();
}

function getImageById($db, $id_image) {
	$tab = array (
		'id'	=> $id_image
	);
	$request = $db->getDB()->prepare("SELECT * FROM generated_images WHERE id = :id");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

function createImage($db, array $infos) {
	$tab = array (
		'id'	=> $infos['id'],
		'url'	=> $infos['url']
	);
	$request = $db->getDB()->prepare("INSERT INTO generated_images (user_id, url, created_at) VALUES (:id, :url, NOW())");
	$request->execute($tab);
	return getImageById($db, $db->getDB()->lastInsertId());
}

function deleteImage($db, $id_image) {
	unlink('imgs/generated/' . getImageById($db, $id_image)['url']);
	$tab = array (
		'id'	=> $id_image
	);
	$request = $db->getDB()->prepare("DELETE FROM generated_images WHERE id = :id");
	$request->execute($tab);
	deleteAllLikesByImage($db, $id_image);
}

function deleteAllLikesByImage($db, $id_image) {
	$tab = array (
		'id'	=> $id_image
	);
	$request = $db->getDB()->prepare("DELETE FROM likes WHERE image_id = :id");
	$request->execute($tab);
}

function addComment($db, array $infos) {
	$tab = array (
		'image'	=> $infos['image_id'],
		'user'	=> $infos['user_id'],
		'mess'	=> htmlspecialchars($infos['message']),
		'now'	=> date("Y-m-d H:i:s", time())
	);
	$request = $db->getDB()->prepare("INSERT INTO comments (user_id, image_id, message, created_at) VALUES (:user, :image, :mess, :now)");
	$request->execute($tab);
	return array ('time' => $tab['now'], 'id' => $db->getDB()->lastInsertId());
}

function getCommentsByImage($db, $id_image) {
	$tab = array (
		'image'	=> $id_image
	);
	$request = $db->getDB()->prepare("SELECT users.id AS user_id, users.pseudo AS username, users.cover AS user_image, comments.message AS message, comments.created_at AS created_at FROM comments, users WHERE comments.image_id = :image AND comments.user_id = users.id ORDER BY comments.created_at ASC");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC);
}

function getUserByImageId($db, $id_image) {
	$tab = array (
		'image'	=> $id_image
	);
	$request = $db->getDB()->prepare("SELECT users.* FROM users INNER JOIN generated_images ON users.id = generated_images.user_id WHERE generated_images.id = :image");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getCommentById($db, $id_comment) {
	$tab = array (
		'comm'	=> $id_comment
	);
	$request = $db->getDB()->prepare("SELECT * FROM comments WHERE id = :comm");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

?>
