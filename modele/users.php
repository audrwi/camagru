<?php

function hashPassword($str) {
	return hash('sha512', hash('whirlpool', $str) . "youwillneverfindthispasswordbitch");
}

function generateToken() {
	return md5(uniqid(mt_rand(), true));
}

function getUserById($db, $id) {
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE id = $id");
	$request->execute();
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getUserByToken($db, $token) {
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE token = :token");
	$request->execute(array ('token' => $token));
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

function changeUserToken($db, $id) {
	$new_token = generateToken();
	$tab = array (
		'id'		=> $id,
		'token'		=> $new_token
	);
	$request = $db->getDB()->prepare("UPDATE users SET token = :token WHERE id = :id");
	$request->execute($tab);
	return $new_token;
}

function changePassword($db, $infos) {
	$tab = array (
		'id'		=> $infos['id'],
		'new'		=> hashPassword($infos['password'])
	);
	$request = $db->getDB()->prepare("UPDATE users SET password = :new WHERE id = :id");
	$request->execute($tab);
}

function getUserByEmail($db, $email) {
	$tab = array (
		'email'		=> $email
	);
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE email = :email");
	$request->execute($tab);
	return $request->fetchAll(PDO::FETCH_ASSOC)[0];
}

function createUser($db, array $infos) {
	$tab = array (
		'pseudo'	=> htmlspecialchars($infos['pseudo']),
		'email'		=> htmlspecialchars($infos['email']),
		'password'	=> hashPassword($infos['password']),
		'token'		=> generateToken()
	);
	$request = $db->getDB()->prepare("INSERT INTO users (pseudo, email, password, active, token, created_at) VALUES (:pseudo, LOWER(:email), :password, 0, :token, NOW())");
	$request->execute($tab);
	return $db->getDB()->lastInsertId();
}

function checkValidUsername($db, $username) {
	$tab = array (
		'pseudo'	=> strtolower($username)
	);
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE LOWER(pseudo) = :pseudo");
	$request->execute($tab);
	return ($request->rowCount() == 0);
}

function checkValidEmail($db, $email) {
	$tab = array (
		'email'		=> strtolower($email)
	);
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE LOWER(email) = :email");
	$request->execute($tab);
	return ($request->rowCount() == 0);
}

function checkUserExists($db, array $infos) {
	$tab = array (
		'login'		=> strtolower($infos['login'])
	);
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE LOWER(pseudo) = :login OR LOWER(email) = :login");
	$request->execute($tab);
	return ($request->rowCount() != 1) ? False : $request->fetch();
}

function checkUserExistsById($db, array $infos) {
	$tab = array (
		'id'		=> $infos['id']
	);
	$request = $db->getDB()->prepare("SELECT * FROM users WHERE id = :id");
	$request->execute($tab);
	return ($request->rowCount() != 1) ? False : $request->fetch();
}

function isRightPassword($real, $password) {
	return ($real == hashPassword($password));
}

function updateActive($db, array $infos) {
	$tab = array (
		'id'		=> $infos['id'],
		'active'	=> $infos['active']
	);
	$request = $db->getDB()->prepare("UPDATE users SET active = :active WHERE id = :id");
	$request->execute($tab);
}

function updateCover($db, array $infos) {
	$tab = array (
		'id'		=> $infos['id'],
		'cover'		=> $infos['cover']
	);
	$request = $db->getDB()->prepare("UPDATE users SET cover = :cover WHERE id = :id");
	$request->execute($tab);
}

?>
