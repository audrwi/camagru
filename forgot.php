<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Validator.class.php');
$check = new Validator();

require_once('class/Form.class.php');
$form = new Forms();

require_once('modele/users.php');

if ($_SESSION['logged'] == True) {
	$_SESSION['logged'] = False;
	$_SESSION['logged_user_id'] = "";
}

$user = checkUserExists($db, array('login' => $_GET['email']));
$check->isValidEmail($_GET['email']);
if ((isset($_GET['email']) && isset($_GET['token']) && (!$user || $check->hasError() || $user['token'] != $_GET['token'] || $user['active'] != 3))) {
	header('Location: ./');
	exit;
}

$state = (!isset($_GET['email']) && !isset($_GET['token']));

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = 'Forgotten password';
		$description = 'Generate a new password.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'forgot';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h2><?php echo $state ? 'Forgotten password' : 'Choose a new password'; ?></h2>
		<form method="POST" onsubmit="<?php echo $state ? 'sendForgotEmail();' : 'changePassword();' ?> return false;" action="">
			<div id="errors"></div>
			<div id="success"></div>
			<?php if ($state) { ?>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "text", "name" => "email", "value" => "", "placeholder" => "Your email address", "id" => "email")); ?>
			</div>
			<?php
				} else {
					echo $form->createInput(array("type" => "hidden", "name" => "token", "value" => $_GET['token'], "id" => "token", "disabled" => "disabled"));
			?>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "text", "name" => "email", "value" => $_GET['email'], "placeholder" => "Your email address", "id" => "email", "disabled" => "disabled")); ?>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "password", "name" => "password", "value" => "", "placeholder" => "New password", "id" => "password")); ?><!--
				 --><p>*<span class="hide">Between 8 and 20 characters.</span></p>
			</div>
			<div class="wrapper_input">
				<?php echo $form->createInput(array("type" => "password", "name" => "password_confirm", "value" => "", "placeholder" => "Confirm your password", "id" => "confirm")); ?><!--
				 --><p>*</p>
			</div>
			<?php
				}
				echo $form->createInput(array("type" => "submit", "value" => $state ? "Reset" : "Change"));
			?>
		</form>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
