<html xmlns="http://www.w3.org/1999/xhtml">
<body bgcolor="#6495ED" style="margin:0px !important; padding:0px; -webkit-text-size-adjust:none;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#6495ED">
	<tr>
		<td>
			<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
				<tr height="10" bgcolor="#6495ED">
					<td colspan="3"></td>
				</tr>
				<tr height="100" valign="center" bgcolor="#242729">
					<td width="20"></td>
					<td width="560" align="center">
						<a href="<?php echo $_GET['url']; ?>" color="#ffffff" title="Camagru" style="color:#ffffff; text-decoration:none;"><font face="Tahoma, Arial, Helvetica, sans-serif" size="6" color="#ffffff">CAMAGRU</font></a>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="20"></td>
					<td width="560" align="left">
						<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#242729">Hello <?php echo $_GET['pseudo']; ?>,<br />You just notice us that you forgot your password. You can reset it by clicking on the following link :</font>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr height="15">
					<td width="20"></td>
					<td width="560" bgcolor="#fbfbfb" style="border: 1px solid #eeeeee; border-bottom: none;" align="center"></td>
					<td width="20"></td>
				</tr>
				<tr valign="center">
					<td width="20"></td>
					<td width="560" bgcolor="#fbfbfb" style="border-left: 1px solid #eeeeee; border-right: 1px solid #eeeeee;" align="center">
						<table align="center" width="560" cellpadding="0" cellspacing="0" border="0" bgcolor="#fbfbfb">
							<tr height="10">
								<td colspan="3"></td>
							</tr>
							<tr>
								<td width="20"></td>
								<td>
									<a href="<?php echo $_GET['url']; ?>forgot-<?php echo $_GET['token']; ?>-<?php echo $_GET['email']; ?>" color="#aaaaaa" title="Camagru" style="color:#aaaaaa;"><font face="Tahoma, Arial, Helvetica, sans-serif" size="2" color="#aaaaaa"><?php echo $_GET['url']; ?>activate-<?php echo $_GET['token']; ?>-<?php echo $_GET['email']; ?></font></a>
								</td>
								<td width="20"></td>
							</tr>
							<tr height="10">
								<td colspan="3"></td>
							</tr>
						</table>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="15">
					<td width="20"></td>
					<td width="560" bgcolor="#fbfbfb" style="border: 1px solid #eeeeee; border-top: none;" align="center"></td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="20"></td>
					<td width="560" align="left">
						<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#242729">Thanks,<br />The Camagru team</font>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr height="10" bgcolor="#242729">
					<td colspan="3"></td>
				</tr>
				<tr height="10" bgcolor="#6495ED">
					<td colspan="3"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
