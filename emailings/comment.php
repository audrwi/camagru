<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href='https://fonts.googleapis.com/css?family=Catamaran:500' rel='stylesheet' type='text/css'>
</head>
<body bgcolor="#6495ED" style="margin:0px !important; padding:0px; -webkit-text-size-adjust:none;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#6495ED">
	<tr>
		<td>
			<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
				<tr height="10" bgcolor="#6495ED">
					<td colspan="3"></td>
				</tr>
				<tr height="100" valign="center" bgcolor="#242729">
					<td width="20"></td>
					<td width="560" align="center">
						<a href="<?php echo $_GET['url']; ?>" color="#ffffff" title="Camagru" style="color:#ffffff; text-decoration:none;"><font face="Tahoma, Arial, Helvetica, sans-serif" size="6" color="#ffffff">CAMAGRU</font></a>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="20"></td>
					<td width="560" align="left">
						<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#242729">Hello <?php echo $_GET['pseudo']; ?>,<br />Someone just comment your image.</font>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="70">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td colspan="3" width="600" bgcolor="#ffffff">
						<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
							<tr height="20">
								<td width="150"></td>
								<td width="300" bgcolor="#00CCFF"></td>
								<td width="150"></td>
							</tr>
							<tr>
								<td width="150"></td>
								<td width="300" bgcolor="#00CCFF" align="center">
									<a href="<?php echo $_GET['url'] . 'image-' . $_GET['image']; ?>" title="See the comment" style="color:#ffffff; text-decoration:none;"><font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#ffffff">SEE THE COMMENT</font></a>
								</td>
								<td width="150"></td>
							</tr>
								<tr height="20">
									<td width="150"></td>
									<td width="300" bgcolor="#00CCFF"></td>
									<td width="150"></td>
								</tr>
						</table>
					</td>
				</tr>
				<tr height="50">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td width="20"></td>
					<td width="560" align="left">
						<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#242729">Thanks,<br />The Camagru team</font>
					</td>
					<td width="20"></td>
				</tr>
				<tr height="30">
					<td colspan="3"></td>
				</tr>
				<tr height="10" bgcolor="#242729">
					<td colspan="3"></td>
				</tr>
				<tr height="10" bgcolor="#6495ED">
					<td colspan="3"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
