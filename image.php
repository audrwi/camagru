<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');
require_once('config/functions.php');

if ($_SESSION['logged'] == True) {
	$me = getUserById($db, $_SESSION['logged_user_id']);
} else {
	header("Location: ./");
	exit;
}

if (!isset($_GET['id']) || $_GET['id'] == "" || !($image = getImageById($db, $_GET['id']))) {
	header("Location: ./");
	exit;
}

$author = getUserById($db, $image['user_id']);
$comments = getCommentsByImage($db, $_GET['id']);
require_once('class/Form.class.php');
$form = new Forms();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = $me['pseudo'] . "'s image";
		$description = 'Comment the ' . $me['pseudo'] . "'s image";
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'image';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<article>
			<a href="profile-<?php echo $author['id']; ?>"></a>
				<div class="author">
					<div>
						<figure>
							<img src="imgs/users/<?php if ($author['cover'] != '') { echo $author['cover']; } else { echo 'placeholder.jpg'; } ?>" alt="<?php echo $author['pseudo']; ?>">
						</figure>
					</div>
					<p><?php echo $author['pseudo']; ?><span>Created the <?php echo date("dS \of F Y", strtotime($author['created_at'])); ?></span></p>
				</div>
			</a>
			<figure class="picture">
				<img src="imgs/generated/<?php echo $image['url']; ?>" alt="<?php echo $author['pseudo']; ?>'s image'">
			</figure><!--
			 --><figcaption>
				<p><?php echo relativeTime(strtotime($image['created_at'])); ?></p>
				<p><span class="like<?php if (isLikedByMe($db, array("user_id" => $_SESSION['logged_user_id'], "image_id" => $image['id']))) { echo ' liked'; } ?>" onclick="likeOrUnlikeImage(this, <?php echo $image['id']; ?>);"><span class="nb-like"><?php echo getLikesById($db, $image['id']); ?></span> <span class="icon-heart"></span></span><span class="comment"><span id="nb-comments"><?php echo getNumberCommentsById($db, $image['id']); ?></span> <span class="icon-bubble"></span></span></p>
			</figcaption>
		</article><!--
		 --><article class="comments">
			<h2>All comments</h2>
			<?php if (getNumberCommentsById($db, $image['id']) == 0) { ?>
			<p class="no-comment" id="no-comment">There is no comment.<span>Be the first to comment this picture.</span></p>
			<?php } ?>
			<div class="scrollbar" id="comments">
				<?php
					if (getNumberCommentsById($db, $image['id']) != 0) {
						foreach($comments as $comment) {
				?>
				<div>
					<a href="profile-<?php echo $comment['user_id']; ?>">
						<figure>
							<img src="imgs/users/<?php if ($comments['user_image'] != '') { echo $comments['user_image']; } else { echo 'placeholder.jpg'; } ?>" alt="<?php echo $comments['username']; ?>">
						</figure>
					</a>
					<p><a href="profile-<?php echo $comment['user_id']; ?>"><?php echo $comment['username']; ?></a><span><?php echo relativeTime(strtotime($comment['created_at'])); ?></span></p>
					<p><span>‟</span><?php echo nl2br(strip_tags($comment['message'])); ?></p>
				</div>
				<?php } } ?>
			</div>
			<form action="image-<?php echo $_GET['id']; ?>" method="POST" onsubmit="addComment(); return false;">
				<textarea name="message" id="message" placeholder="Your comment here..."></textarea><!--
				 --><?php echo $form->createInput(array("type" => "submit", "value" => "Send my message")); ?>
			</form>
		</article>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
