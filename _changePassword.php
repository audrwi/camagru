<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('class/Validator.class.php');
$check = new Validator();

require_once('modele/users.php');

$check->isValidEmail($_POST['email']);
if (!$check->hasError() && checkValidEmail($db, $_POST['email']))
	$check->setError("This email address doesn't exist.");
if (!($user = getUserByToken($db, $_POST['token'])) || $user['email'] != $_POST['email'])
	$check->setError("Invalid informations.");
$check->isValidPassword($_POST['pass'], $_POST['conf']);
if (!$check->hasError() && $user['active'] == 0)
	$check->setError("This account is not activated.");
else if (!$check->hasError() && $user['active'] == 1)
	$check->setError("No email sent.");
else if (!$check->hasError() && $user['active'] == 2)
	$check->setError("This account has been deleted.");

if (!$check->hasError()) {
	changePassword($db, array ('id' => $user['id'], 'password' => $_POST['pass']));
	updateActive($db, array ('id' => $user['id'], 'active' => 1));
}

if ($check->hasError())
	echo implode(';', $check->getErrors());

?>
