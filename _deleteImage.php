<?php

session_start();

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');

if (checkUserExistsById($db, array("id" => $_SESSION['logged_user_id'])) && $_SESSION['logged_user_id'] == getImageById($db, $_POST['image_id'])['user_id'])
	deleteImage($db, $_POST['image_id']);

?>
