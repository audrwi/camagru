<?php

session_start();

require_once('class/Database.class.php');
$db = new Database('camagru');

$image = base64_decode($_POST['image']);
if (!is_dir('imgs/generated'))
	mkdir('imgs/generated');
$url = time() . '-' . $_SESSION['logged_user_id'] . '.png';
$long_url = 'imgs/generated/' . $url;
file_put_contents($long_url, $image);

require_once('modele/images.php');

$pic = createImage($db, array ('id' => $_SESSION['logged_user_id'], 'url' => $url));
$class = (isLikedByMe($db, array("user_id" => $_SESSION['logged_user_id'], "image_id" => $pic['id']))) ? " liked" : "";

echo '<div>
	<a href="image-' . $pic['id'] . '">
		<figure>
			<img src="imgs/generated/' . $pic['url'] . '">
		</figure>
	</a><!--
	--><figcaption><p><span class="icon-cross" onclick="deleteImage(this, ' . $pic['id'] . ');"></span></p><p><span class="like' . $class . '" onclick="likeOrUnlikeImage(this, ' . $pic['id'] . ');"><span class="nb-like">' . getLikesById($db, $pic['id']) . '</span> <span class="icon-heart"></span></span><span class="comment">' . getNumberCommentsById($db, $pic['id']) . ' <span class="icon-bubble"></span></span></p></figcaption>
</div>';

?>
