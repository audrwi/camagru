<?php

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');

if ($_SESSION['logged'] == True) {
	$_SESSION['logged'] = False;
	$_SESSION['logged_user_id'] = "";
}

if (!isset($_GET['token'])) {
	header('Location: ./');
	exit;
}

$me = getUserByToken($db, $_GET['token']);
if (!isset($me['id']))
	$error = "No account to activate.";
else if ($me['active'] == 1 || $me['active'] == 3)
	$error = "This account is already activated.";
else if ($me['active'] == 2)
	$error = "This account has been deleted.";
else
	updateActive($db, array ('id' => $me['id'], 'active' => 1));

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$title = 'Activate your account';
		$description = 'Thanks to activate your account. You can login.';
		include('includes/head.inc.php');
	?>
</head>
<body>
	<?php
		$page = 'activate';
		include('includes/header.inc.php');
	?>
	<section id="content" class="<?php echo $page; ?>">
		<h2>Activate your account</h2>
		<?php if (isset($error) && $error != "") { ?>
			<p><?php echo $error; ?></p>
		<?php } else { ?>
			<p>You successfully activated your account.</p>
			<p>Now you can <a href="login">login</a>.</p>
		<?php } ?>
	</section>
	<?php include('includes/footer.inc.php'); ?>
</body>
</html>
