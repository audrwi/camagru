<header>
	<a href="./"><h1>Camagru</h1></a><!--
	--><nav>
		<?php if ($_SESSION['logged'] == True) { ?>
			<a href="./" <?php if ($page == 'index') { echo 'class="active"'; } ?>>Take a picture</a><!--
			--><a href="gallery" <?php if ($page == 'gallery') { echo 'class="active"'; } ?>>Gallery</a><!--
			--><div>
				<figure>
					<img src="imgs/users/<?php if ($me['cover'] != '') { echo $me['cover']; } else { echo 'placeholder.jpg'; } ?>" alt="<?php echo $me['pseudo']; ?>" onclick="toggle_visibility('user_nav');">
				</figure>
			</div>
			<div id="user_nav">
				<div class="line"></div>
				<a href="profile-<?php echo $me['id']; ?>">My profile</a>
				<a href="logout">Logout</a>
			</div>
		<?php } else { ?>
			<a href="register" class="unlog">Register</a><!--
			--><a href="login" class="unlog">Login</a>
		<?php } ?>
	</nav>
</header>
