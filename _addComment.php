<?php

session_start();

require_once('class/Database.class.php');
$db = new Database('camagru');

require_once('modele/users.php');
require_once('modele/images.php');
require_once('config/functions.php');

if (isset($_POST) && $_POST['message'] != "" && $_POST['message'] && strlen(trim($_POST['message'])) > 0) {
	$array = addComment($db, array("image_id" => $_POST['image'], "user_id" => $_SESSION['logged_user_id'], "message" => $_POST['message']));
}

$user = getUserById($db, $_SESSION['logged_user_id']);
$cover = ($user['cover'] != '') ? $user['cover'] : 'placeholder.jpg';

$author = getUserByImageId($db, $_POST['image']);
$start = 'http://' . str_replace("_addComment.php", "", $_SERVER[HTTP_HOST] . $_SERVER['REQUEST_URI']);
$header = 'From: "Camagru" <aboucher@student.42.fr>' . "\r\n";
$header .= 'Reply-To: <aboucher@student.42.fr>' . "\r\n";
$header .= 'MIME-Version: 1.0' . "\r\n";
$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$content = file_get_contents($start . 'emailings/comment.php?pseudo=' . $author['pseudo'] . '&image=' . $_POST['image'] . '&url=' . $start);
mail($author['email'], "A new comment on your picture", $content, $header);

echo '<div>
	<a href="profile-' . $user['id'] . '">
		<figure>
			<img src="imgs/users/' . $cover . '" alt="' . $user['pseudo'] . '">
		</figure>
	</a>
	<p><a href="profile-' . $user['id'] . '">' . $user['pseudo'] . '</a><span>' . relativeTime(strtotime($array['time'])) . '</span></p>
	<p><span>‟</span>' . nl2br(strip_tags(htmlspecialchars($_POST['message']))) . '</p>
</div>';

?>
